package com.bigfans.cartservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.api.clients.ServiceClient;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-16 下午3:06
 **/
@Component
public class InventoryServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Integer> getSurplusStock(String prodId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            Integer stock = serviceClient.get(Integer.class , "http://catalog-service/stock?prodId={prodId}" , prodId);
            return stock;
        });
    }

}
