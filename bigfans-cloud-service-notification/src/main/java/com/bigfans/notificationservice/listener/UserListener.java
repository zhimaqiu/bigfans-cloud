package com.bigfans.notificationservice.listener;

import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaListener;
import com.bigfans.framework.plugins.SmsPlugin;
import com.bigfans.model.event.notification.RequestVCodeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@KafkaConsumerBean
public class UserListener {

    @Autowired
    private SmsPlugin smsPlugin;

    @KafkaListener
    public void on(RequestVCodeEvent event){
        String mobile = event.getMobile();
        String vcode = event.getCode();
        // TODO 版本太老,以前的api发送不了了,有时间会更新
        smsPlugin.sendVerificationCode(mobile , vcode);
    }

}
