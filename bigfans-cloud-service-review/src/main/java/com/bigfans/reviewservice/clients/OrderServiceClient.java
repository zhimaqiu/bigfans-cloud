package com.bigfans.reviewservice.clients;

import com.bigfans.Constants;
import com.bigfans.api.clients.ServiceClient;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.reviewservice.model.dto.Order;
import com.bigfans.reviewservice.model.dto.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Component
public class OrderServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Order> getOrder(String orderId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate , token);
            Map data = serviceClient.get(Map.class , "http://order-service/orders/{orderId}" , orderId);
            Order category = BeanUtils.mapToModel(data, Order.class);
            return category;
        });
    }

    public CompletableFuture<List<OrderItem>> getOrderItems(String orderId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate , token);
            List data = serviceClient.get(List.class , "http://order-service/orderItems?orderId={orderId}" , orderId);
            List<OrderItem> items = new ArrayList<>();
            for(int i = 0;i<data.size() ; i++){
                OrderItem orderItem = BeanUtils.mapToModel((Map)data.get(i), OrderItem.class);
                items.add(orderItem);
            }
            return items;
        });
    }
}
