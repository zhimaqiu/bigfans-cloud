package com.bigfans.reviewservice.clients;

import com.bigfans.Constants;
import com.bigfans.api.clients.ServiceClient;
import com.bigfans.framework.exception.ServiceRuntimeException;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.reviewservice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Component
public class UserServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<User> getUser(String userId) throws ServiceRuntimeException {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate , token);
            Map data = serviceClient.get(Map.class , "http://user-service/users/{userId}" , userId);
            User user = BeanUtils.mapToModel(data, User.class);
            return user;
        });
    }

}
