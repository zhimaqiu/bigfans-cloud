package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.SpecOption;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年5月31日下午9:11:57
 *
 */
public interface SpecOptionDAO extends BaseDAO<SpecOption>{
	
	/**
	 * 查询分类下的所有的规格项
	 * @param catId
	 * @return
	 */
	List<SpecOption> listByCatId(String catId, Long start, Long pagesize);

	/**
	 * 查询分类下的所有的规格项
	 * @param catId
	 * @return
	 */
	List<SpecOption> listByCatIds(List<String> catId, Long start, Long pagesize);

	/**
	 * 查询商品组下的所有的规格项
	 * @param pgId
	 * @return
	 */
	List<SpecOption> listByPgId(String pgId, Long start, Long pagesize);

	/**
	 * 查询商品下的所有的规格项
	 * @param pid
	 * @return
	 */
	List<SpecOption> listByPid(String pid, Long start, Long pagesize);

	/**
	 * 查询商品下的所有的规格项
	 * @param pid
	 * @return
	 */
	List<SpecOption> list(String catId, String pgId, String pid, Long start, Long pagesize);
}
