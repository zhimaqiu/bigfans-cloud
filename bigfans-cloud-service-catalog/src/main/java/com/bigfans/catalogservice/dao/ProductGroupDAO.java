package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.ProductGroup;
import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.framework.model.PageBean;

public interface ProductGroupDAO extends BaseDAO<ProductGroup> {

	PageBean<ProductGroup> pageByCategoryId(String[] categoryId, Long start, Long pagesize);

	String getDescription(String pgId , String prodId);

}
