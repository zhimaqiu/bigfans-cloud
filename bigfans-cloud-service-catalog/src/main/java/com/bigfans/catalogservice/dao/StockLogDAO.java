package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.StockLog;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong 2015年5月31日下午9:11:57
 *
 */
public interface StockLogDAO extends BaseDAO<StockLog> {

    List<StockLog> listByOrder(String orderId);

    List<StockLog> listByOrder(String orderId , String direction);

    Integer countByOrder(String orderId);

}
