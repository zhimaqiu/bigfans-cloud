package com.bigfans.catalogservice.service.spec;

import com.bigfans.catalogservice.model.SpecOption;
import com.bigfans.framework.dao.BaseService;

import java.util.List;

/**
 * 
 * @Description:Spec SpecOption
 * @author lichong
 * 2015年5月30日下午10:56:34
 *
 */
public interface SpecOptionService extends BaseService<SpecOption> {

	/**
	 * 查询商品的规格项和规格值
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	List<SpecOption> listByPid(String pid , Long start , Long pagesize) throws Exception;
	
	/**
	 * 查询商品组下的所有option
	 * @param pgid
	 * @return
	 * @throws Exception
	 */
	List<SpecOption> listByPgId(String pgid, Long start , Long pagesize) throws Exception;
	
	/**
	 * 查询分类下的所有的规格项
	 * @param catId
	 * @return
	 * @throws Exception
	 */
	List<SpecOption> listByCatId(String catId, Long start , Long pagesize) throws Exception;

	List<SpecOption> list(String catId , String pgId ,String pid ,  Long start , Long pagesize) throws Exception;
	
}
