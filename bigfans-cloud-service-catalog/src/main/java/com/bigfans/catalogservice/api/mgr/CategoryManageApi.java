package com.bigfans.catalogservice.api.mgr;

import com.bigfans.catalogservice.model.Category;
import com.bigfans.catalogservice.service.category.CategoryService;
import com.bigfans.framework.annotations.NeedLogin;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoryManageApi {

	@Autowired
	private CategoryService categoryService;
	
	@PostMapping(value = "/category")
	@NeedLogin
	public RestResponse create(@RequestBody Category category) throws Exception{
		categoryService.create(category);
		return RestResponse.ok();
	}

	@GetMapping(value = "/categories")
	@NeedLogin
	public RestResponse list() throws Exception{
		List<Category> categories = categoryService.listByLevel(1, true);
		return RestResponse.ok(categories);
	}

	@GetMapping(value = "/categories/{catId}")
	@NeedLogin
	public RestResponse getById(@PathVariable(value = "catId") String catId) throws Exception{
		Category category = categoryService.load(catId);
		return RestResponse.ok(category);
	}
}
