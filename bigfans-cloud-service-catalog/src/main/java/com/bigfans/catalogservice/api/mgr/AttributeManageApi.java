package com.bigfans.catalogservice.api.mgr;

import com.bigfans.catalogservice.model.AttributeOption;
import com.bigfans.catalogservice.model.AttributeValue;
import com.bigfans.catalogservice.service.attribute.AttributeOptionService;
import com.bigfans.catalogservice.service.attribute.AttributeValueService;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AttributeManageApi extends BaseController {

    @Autowired
    private AttributeValueService attributeValueService;
    @Autowired
    private AttributeOptionService attributeOptionService;

    @PostMapping(value = "/attributeOption")
    public RestResponse createAttributeOption(@RequestBody AttributeOption option) throws Exception {
        attributeOptionService.create(option);
        return RestResponse.ok();
    }
}
