package com.bigfans.searchservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.api.clients.ServiceClient;
import com.bigfans.framework.exception.RemoteServiceRuntimeException;
import com.bigfans.framework.exception.ServiceRuntimeException;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.utils.CollectionUtils;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.framework.web.RestResponseError;
import com.bigfans.framework.web.RestResponseOk;
import com.bigfans.searchservice.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class CatalogServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Category> getCategory(String catId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            Map data = serviceClient.get(Map.class , "http://catalog-service/categories/{catId}" , catId);
            Category category = BeanUtils.mapToModel(data, Category.class);
            return category;
        });
    }

    public CompletableFuture<Product> getProduct(String prodId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            Map data = serviceClient.get(Map.class , "http://catalog-service/product/{prodId}" , prodId);
            Product product = BeanUtils.mapToModel(data, Product.class);
            return product;
        });
    }

    public CompletableFuture<List<ProductAttribute>> getAttributesByProductId(String prodId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            List data = serviceClient.get(List.class , "http://catalog-service/attributes?prodId={prodId}" , prodId);
            List<ProductAttribute> attributes = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                Map attrMap = (Map) data.get(i);
                ProductAttribute attr = new ProductAttribute();
                attr.setOptionId((String) attrMap.get("option_id"));
                attr.setOptionName((String) attrMap.get("option_name"));
                attr.setValueId((String) attrMap.get("id"));
                attr.setValue((String) attrMap.get("value"));
                attributes.add(attr);
            }
            return attributes;
        });
    }

    public CompletableFuture<List<ProductSpec>> getSpecsByProductId(String prodId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            List data = serviceClient.get(List.class , "http://catalog-service/specs?prodId={prodId}" , prodId);
            List<ProductSpec> specs = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                Map m = (Map) data.get(i);
                ProductSpec spec = BeanUtils.mapToModel(m, ProductSpec.class);
                specs.add(spec);
            }
            return specs;
        });
    }

    public CompletableFuture<List<Tag>> getTagsByProductId(String prodId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            List data = serviceClient.get(List.class , "http://catalog-service/tags?prodId={prodId}" , prodId);
            List<Tag> tags = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                Map m = (Map) data.get(i);
                Tag t = BeanUtils.mapToModel(m, Tag.class);
                tags.add(t);
            }
            return tags;
        });
    }

    public CompletableFuture<Brand> getBrand(String brandId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            Map data = serviceClient.get(Map.class , "http://catalog-service/brands/{brandId}" , brandId);
            Brand brand = BeanUtils.mapToModel(data, Brand.class);
            return brand;
        });
    }

    public CompletableFuture<Tag> getTag(String tagId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            Map data = serviceClient.get(Map.class , "http://catalog-service/tags/{tagId}" , tagId);
            Tag tag = BeanUtils.mapToModel(data, Tag.class);
            return tag;
        });
    }

    public CompletableFuture<List<AttributeValue>> getAttributesByIdList(List<String> idList) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            String ids = CollectionUtils.join(idList, ",");
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            List data = serviceClient.get(List.class , "http://catalog-service/attributes?ids={ids}" , ids);
            List<AttributeValue> attributeValues = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                Map m = (Map) data.get(i);
                AttributeValue attributeValue = BeanUtils.mapToModel(m, AttributeValue.class);
                attributeValues.add(attributeValue);
            }
            return attributeValues;
        });
    }
}
