package com.bigfans.pricingservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.api.clients.ServiceClient;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.pricingservice.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class CatalogServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Product> getProductById(String prodId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate , token);
            Map data = serviceClient.get(Map.class , "http://catalog-service/attributes?prodId={prodId}" , prodId);
            Product product = BeanUtils.mapToModel(data , Product.class);
            return product;
        });
    }
}
