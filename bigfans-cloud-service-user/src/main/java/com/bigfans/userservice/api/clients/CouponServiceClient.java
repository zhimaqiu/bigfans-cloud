package com.bigfans.userservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.api.clients.ServiceClient;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.web.CookieHolder;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.userservice.model.Coupon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Component
public class CouponServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Coupon> getCoupon(String couponId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate , token);
            Map data = serviceClient.get(Map.class , "http://pricing-service/coupons/{id}" , couponId);
            Coupon coupon = BeanUtils.mapToModel(data , Coupon.class);
            return coupon;
        });
    }
}
