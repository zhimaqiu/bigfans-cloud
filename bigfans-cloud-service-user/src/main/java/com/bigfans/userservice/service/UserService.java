package com.bigfans.userservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.userservice.model.User;

import java.math.BigDecimal;

public interface UserService extends BaseService<User> {
	
	/**
	 * 判断当前用户是否存在
	 * @param user
	 * @return
	 * @throws Exception 
	 */
	boolean exist(User user) throws Exception;
	
	/**
	 * 判断当前邮箱是否已经使用
	 * @param email
	 * @return
	 * @throws Exception 
	 */
	boolean emailExist(String email) throws Exception;
	
	/**
	 * 判断当前用户名是否存在
	 * @param account
	 * @return
	 * @throws Exception 
	 */
	boolean accountExist(String account) throws Exception;
	
	/**
	 * 注册用户
	 * @param user
	 * @return 
	 * @throws Exception 
	 */
	User regist(User user) throws Exception;
	
	/**
	 * 通过账号或者邮箱登陆
	 * @param account 账号或邮箱
	 * @param password
	 * @return
	 */
	User login(String account, String password) throws Exception;
	
	/**
	 * 通过账号或者邮箱登陆
	 * @param account 账号或邮箱
	 * @param password
	 * @param md5 是否进行md5加密
	 * @return
	 * @throws Exception 
	 */
	User login(String account, String password, boolean md5) throws Exception;
	
	/**
	 * 激活账号
	 * @param user
	 * @return
	 */
	User activate(User user) throws Exception;

	void usePoints(String userId, Float points) throws Exception;
	
	void useBalance(String userId, BigDecimal balance) throws Exception;
	
}
