package com.bigfans.orderservice.api.clients;

import com.bigfans.orderservice.model.PayMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class PayMethodServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<PayMethod> getPayMethod(String methodId){
        return CompletableFuture.supplyAsync(() -> {
            return new PayMethod();
        });
    }
}
