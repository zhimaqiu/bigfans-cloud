package com.bigfans.orderservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.api.clients.ServiceClient;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.orderservice.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class UserServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Void> useProperty(String orderId, String couponId, Float points, BigDecimal balance) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            Map<String, Object> payload = new HashMap<>();
            payload.put("id", orderId);
            payload.put("couponId", couponId);
            payload.put("points", points);
            payload.put("balance", balance);
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            serviceClient.post("http://user-service/useProperty" , "" , payload);
            return null;
        });
    }

    public CompletableFuture<Address> myAddress(String addressId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            Map data = serviceClient.get(Map.class , "http://user-service/myAddress/{addressId}" , addressId);
            Address address = BeanUtils.mapToModel(data, Address.class);
            return address;
        });
    }
}
