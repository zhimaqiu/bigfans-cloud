package com.bigfans.orderservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.api.clients.ServiceClient;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.utils.JsonUtils;
import com.bigfans.framework.web.CookieHolder;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.orderservice.model.Product;
import com.bigfans.orderservice.model.ProductSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class CatalogServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Product> getProductById(String prodId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            Map data = serviceClient.get(Map.class , "http://catalog-service/product/{prodId}" , prodId);
            List specList = (List)data.get("specList");
            List<ProductSpec> specs = new ArrayList<>();
            for(int i = 0;i<specList.size();i++){
                ProductSpec spec = BeanUtils.mapToModel((Map)specList.get(i) , ProductSpec.class);
                specs.add(spec);
            }
            Product product = BeanUtils.mapToModel((Map) data, Product.class);
            product.setSpecs(JsonUtils.toJsonString(specs));
            return product;
        });
    }

    public CompletableFuture<String> orderStockOut(String orderId, Map<String, Integer> prodQuantityMap) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            serviceClient.post("http://catalog-service/orderStockOut?id={id}" , prodQuantityMap ,orderId);
            return null;
        });
    }

    public CompletableFuture<List<ProductSpec>> getSpecsByProductId(String prodId) {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            List list = serviceClient.get(List.class , "http://catalog-service/specs?prodId={prodId}" , prodId);
            List<ProductSpec> specs = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                ProductSpec productSpec = BeanUtils.mapToModel((Map) list.get(i), ProductSpec.class);
                specs.add(productSpec);
            }
            return specs;
        });
    }

//    public CompletableFuture<String> getTagsByProductId(String prodId) {
//        return CompletableFuture.supplyAsync(() -> {
//            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/tags?prodId={prodId}").build().expand(prodId).encode();
//            ResponseEntity<String> responseEntity = restTemplate.getForEntity(builder.toUri(), String.class);
//            String jsonData = responseEntity.getBody();
//            return jsonData;
//        });
//    }
}
