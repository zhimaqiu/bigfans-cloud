package com.bigfans.orderservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.api.clients.ServiceClient;
import com.bigfans.framework.utils.CollectionUtils;
import com.bigfans.framework.web.CookieHolder;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.orderservice.model.OrderCheckoutItem;
import com.bigfans.orderservice.model.OrderItemSpec;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class CartServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "getCheckoutItemsFallback")
    public CompletableFuture<List<OrderCheckoutItem>> getCheckoutItems() {
        String token = RequestHolder.getHeader(Constants.TOKEN.HEADER_KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            ServiceClient serviceClient = new ServiceClient(restTemplate, token);
            List items = serviceClient.get(List.class , "http://cart-service/checkoutItems");
            List<OrderCheckoutItem> orderItems = new ArrayList<>();
            items.forEach((item) -> {
                OrderCheckoutItem orderItem = new OrderCheckoutItem();
                Map data = (Map)item;
                orderItem.setProdId((String) data.get("prodId"));
                orderItem.setQuantity((Integer) data.get("quantity"));
                orderItem.setProdName((String) data.get("prodName"));
                orderItem.setProdImg((String) data.get("prodImg"));

                List specs = (List) data.get("specList");
                if(CollectionUtils.isNotEmpty(specs)){
                    specs.forEach((spec) -> {
                        Map specMap = (Map)spec;
                        OrderItemSpec orderItemSpec = new OrderItemSpec();
                        orderItemSpec.setOption((String)specMap.get("option"));
                        orderItemSpec.setValue((String)specMap.get("value"));
                        orderItem.addSpec(orderItemSpec);
                    });
                }
                orderItems.add(orderItem);
            });
            return orderItems;
        });
    }

    public CompletableFuture<String> getCheckoutItemsFallback() {
        return CompletableFuture.supplyAsync(() -> {
            return "";
        });
    }
}
