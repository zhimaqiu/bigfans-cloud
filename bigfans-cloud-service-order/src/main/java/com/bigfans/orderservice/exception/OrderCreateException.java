package com.bigfans.orderservice.exception;

import com.bigfans.framework.exception.ServiceRuntimeException;

/**
 * @author lichong
 * @create 2018-02-26 下午10:46
 **/
public class OrderCreateException extends ServiceRuntimeException {

    public OrderCreateException() {
    }

    public OrderCreateException(Throwable e) {
        super(e);
    }

    public OrderCreateException(String message) {
        super(message);
    }

    public OrderCreateException(Integer errorCode, String message) {
        super(errorCode, message);
    }
}
